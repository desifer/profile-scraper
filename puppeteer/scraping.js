module.exports = async function (page, selectors) {
    let extracted = {}
    let allExtracted = []
    for (let [field, data] of Object.entries(selectors)) {
        const { value } = data;
        extracted[`${field}`] = await page.$eval(value, el => el.textContent.trim());
    }
    allExtracted.push(extracted);

    return allExtracted;
}
