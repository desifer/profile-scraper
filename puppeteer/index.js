const dotenv = require("dotenv");
const puppeteer = require("puppeteer");
const ObjectsToCsv = require('objects-to-csv');
const chalk = require("chalk");
const { selectors } = require(".//datamodel");
const scraping = require("./scraping");

async function main() {
    const { EXTRACT_URL, DEBUG } = process.env;
    const headless = typeof DEBUG === "undefined" || DEBUG == false;

    // open browser
    const browser = await puppeteer.launch({ headless });

    // open page
    const page = await browser.newPage();
    await page.setViewport({ width: 1280, height: 800 });
    await page.goto(EXTRACT_URL);

    // extract all the values now
    const data = await scraping(page, selectors);
    console.log(chalk.yellow("extracted: "), data);

    // cleanup & steps screenshots
    browser.close();

    const csv = new ObjectsToCsv(data);

    // Save to file:
    await csv.toDisk('./test.csv');

    // Return the CSV file as string:
    console.log(await csv.toString());toString

}

// load .env variables, console.log(process.env);
const envConfigurationResult = dotenv.config();
if (envConfigurationResult.error) throw envConfigurationResult.error;

main();