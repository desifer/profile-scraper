module.exports = {
    selectors: {
        fullName:{
            title: "Full name",
            value: '#main-content > section.core-rail.mx-auto.papabear\\:w-core-rail-width.mamabear\\:max-w-\\[790px\\].babybear\\:max-w-\\[790px\\] > div > section > section.top-card-layout.container-lined.overflow-hidden.babybear\\:rounded-\\[0px\\] > div > div.top-card-layout__entity-info-container.flex.flex-wrap.papabear\\:flex-nowrap > div:nth-child(1) > h1',
        } ,
        position:{
            title:"Position",
            value: '#main-content > section.core-rail.mx-auto.papabear\\:w-core-rail-width.mamabear\\:max-w-\\[790px\\].babybear\\:max-w-\\[790px\\] > div > section > section.top-card-layout.container-lined.overflow-hidden.babybear\\:rounded-\\[0px\\] > div > div.top-card-layout__entity-info-container.flex.flex-wrap.papabear\\:flex-nowrap > div:nth-child(1) > h2',
        } ,
        place: {
            title: "Place",
            value: '#main-content > section.core-rail.mx-auto.papabear\\:w-core-rail-width.mamabear\\:max-w-\\[790px\\].babybear\\:max-w-\\[790px\\] > div > section > section.top-card-layout.container-lined.overflow-hidden.babybear\\:rounded-\\[0px\\] > div > div.top-card-layout__entity-info-container.flex.flex-wrap.papabear\\:flex-nowrap > div:nth-child(1) > h3 > div',
        }
    }
}

